#!flask/bin/python

# Imports the app variable from our app package;
# Invokes its run method to start the server.
from app import app
app.run(debug=True)