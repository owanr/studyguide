# render_template takes a template filename and a variable list of template arguments
# and returns the rendered template, with all the arguments replaced
from flask import render_template
from app import app

@app.route('/studyguide')
@app.route('/mystudyguide')
def index():
	user = {'fname': 'Risako'}
	return render_template('index.html',
						   title='Home',
						   user=user)

@app.route('/mystudyguide/biginteger')
def biginteger():
	biginteger = [
	]
	return render_template('Methods/biginteger.html',
						   title='BigInteger',
						   biginteger=biginteger)

@app.route('/mystudyguide/database')
def database():
	return render_template('database.html',
						   title='Databases')

@app.route('/mystudyguide/hashmap')
def hashmap():
	hashmap = [
		{ 
			'method': 'put', 
			'sourcecode': 'put source code',
			'keypoints': 'put key points'
		},
		{ 
			'method': 'containsKey', 
			'sourcecode': 'containsKey source code',
			'keypoints': 'containsKey key points'
		}
	]
	return render_template('Methods/hashmap.html',
						   title='HashMap',
						   hashmap=hashmap)

@app.route('/mystudyguide/reflection')
def reflection():
	reflection = [
	]
	return render_template('reflection.html',
						   title='Reflection',
						   reflection=reflection)